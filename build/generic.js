"use strict";
function getData(value) {
    return value;
}
console.log(getData("Husni").length);
console.log(getData(123).length);
// Generic
function myData(value) {
    return value;
}
console.log(myData("Husni").length);
console.log(myData(123));
const arrowFunc = (value) => {
    return value;
};
console.log(arrowFunc("RezkyM").length);
console.log(arrowFunc(123));
