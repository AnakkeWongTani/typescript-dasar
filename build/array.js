"use strict";
// array
let array;
array = [1, 2, 3, 4];
let array2;
array2 = ['string1', 'string2'];
let array3;
array3 = [1, "string", true, {}];
// tuples
let biodata;
biodata = ["Surabaya", 123];
// biodata = ["Malang", true]
// biodata = ["Sidoarjo", 100, false]
