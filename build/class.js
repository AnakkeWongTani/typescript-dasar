"use strict";
// export class User {
//     constructor(public name: string) {
//     }
// }
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = void 0;
// let user = new User("Husni")
class User {
    constructor(name, age) {
        this.age = age;
        this.getName = () => {
            return this.name;
        };
        this.name = name;
    }
    setName(value) {
        this.name = value;
    }
}
exports.User = User;
// let user = new User("Husni", 39)
// console.log(user)
class Admin extends User {
    constructor(phone, name, age) {
        super(name, age);
        this.read = true;
        this.write = true;
        this._email = "";
        this.phone = phone;
    }
    static getNameRole() {
        return "Joss";
    }
    getRole() {
        return {
            read: this.read,
            write: this.write
        };
    }
    set email(value) {
        if (value.length < 5) {
            this._email = "email salah";
        }
        else {
            this._email = value;
        }
    }
    get email() {
        return this._email;
    }
}
Admin.getRoleName = "Admin";
// let admin = new Admin("123456", "Rezky", 9)
// admin.getName()
// admin.getRole()
// admin.setName("sarry")
// console.log(admin.phone)
// admin.email = "husn"
// console.log(admin.email)
let admin = Admin.getNameRole();
console.log(admin);
