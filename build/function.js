"use strict";
function getName() {
    return "Hallo nama saya REMIRA";
}
console.log(getName());
// fungsi tanpa kembalian return
const printName = () => {
    console.log("tanpa return");
};
printName();
const multiply = (val1, val2) => {
    return val1 * val2;
};
// argument types
const result = multiply(20, 5);
console.log(result);
const Add = (val1, val2) => {
    return val1 + val2;
};
// default parameter
const fullName = (first, last = "Mubarok") => {
    return first + " " + last;
};
console.log(fullName("Husni"));
// optional parameter, hanya bisa di tipe data string
const getUmur = (val1, val2) => {
    return val1 + " " + val2;
};
console.log(getUmur("A"));
