// numeric enums
enum Month {
    JAN = 1,
    FEB,
    MAR,
    APR,
    MAY
}

console.log(Month.FEB);

// string enums
enum MonthString {
    JAN = "Januari",
    FEB = "Februari",
    MAR = "Maret",
    APR = "April",
    MAY = "Mei"
}

console.log(MonthString.FEB);