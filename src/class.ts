// export class User {
//     constructor(public name: string) {
//     }
// }

// let user = new User("Husni")

export class User {
    public name: string

    constructor(name: string, public age: number) {
        this.name = name
    }

    setName(value: string): void {
        this.name = value
    }

    getName = (): string => {
        return this.name
    }
}

// let user = new User("Husni", 39)
// console.log(user)

class Admin extends User {
    read: boolean = true
    write: boolean = true
    phone: string
    private _email: string = ""
    static getRoleName: string = "Admin"

    constructor(phone: string, name: string, age: number) {
        super(name, age)
        this.phone = phone
    }

    static getNameRole() {
        return "Mantap"
    }

    getRole(): {read: boolean, write: boolean} {
        return {
            read: this.read,
            write: this.write
        }
    }

    set email(value: string) {
        if (value.length < 5) {
            this._email = "email salah"
        } else {
            this._email = value
        }
    }

    get email(): string {
        return this._email
    }
}

// let admin = new Admin("123456", "Rezky", 9)
// admin.getName()
// admin.getRole()
// admin.setName("sarry")
// console.log(admin.phone)

// admin.email = "husn"
// console.log(admin.email)

let admin = Admin.getNameRole()
console.log(admin)


