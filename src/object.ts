// Object

type User = {
    name: string,
    age: number
}

let user : User = {
    name: "Husni",
    age: 39
}

user = {
    name: "Joko",
    age: 40
}