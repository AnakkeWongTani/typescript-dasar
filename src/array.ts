// array
let array: number[];
array = [1, 2, 3, 4]

let array2: string[]
array2 = ['string1', 'string2']

let array3: any[]
array3 = [1, "string", true, {}]

// tuples
let biodata: [string, number]
biodata = ["Cikarang", 123]
// biodata = ["Malang", true]
// biodata = ["Sidoarjo", 100, false]