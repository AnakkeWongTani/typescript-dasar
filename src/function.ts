function getName(): string {
    return "Hallo nama saya REMIRA"
}

console.log(getName());

// fungsi tanpa kembalian return

const printName = (): void => {
    console.log("tanpa return");
    
}

printName()

const multiply = (val1 :number, val2: number): number => {
    return val1 * val2
}

// argument types

const result = multiply(20, 5)
console.log(result)

// function as type
type Tambah = (val1: number, val2: number) => number

const Add: Tambah = (val1: number, val2: number): number => {
    return val1 + val2
}

// default parameter
const fullName = (first: string, last: string = "Mubarok"): string =>{
    return first + " " + last
}

console.log(fullName("Husni"))

// optional parameter, hanya bisa di tipe data string

const getUmur = (val1: string, val2?: string): string => {
    return val1 + " " + val2
}

console.log(getUmur("A"))

