// string
let nama: string = "husni"
nama = "mubarok"

// number
let umur: number
umur = 30
umur = 25

// boolean
let isMarried: boolean
isMarried = true
isMarried = false

// any
let heroes: any = "Iron Man"
heroes = 20
heroes = []
heroes ={}
heroes = true || false || "string"

// union type
62812345
"0812345"

let phone: number | string
phone = 62812345
phone = "0812345"