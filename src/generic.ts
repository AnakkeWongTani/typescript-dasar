function getData(value: any) {
    return value
}

console.log(getData("Husni").length)
console.log(getData(123).length)

// Generic
function myData<T>(value: T) {
    return value
}

console.log(myData("Husni").length)
console.log(myData(123))

const arrowFunc = <T> (value: T) => {
    return value
}

console.log(arrowFunc("RezkyM").length)
console.log(arrowFunc(123))